﻿// Type: DrawGizmo
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class DrawGizmo : MonoBehaviour
{
  public Color BoxColor;

  public DrawGizmo()
		:base()
  {
    this.BoxColor = Color.blue;
  }

  public virtual void Start()
  {
  }

  public virtual void Update()
  {
  }

  public virtual void OnDrawGizmosSelected()
  {
    Gizmos.color = (this.BoxColor);
    Gizmos.DrawCube(((Component) this).transform.position, Vector3.one);
  }

  public virtual void Main()
  {
  }
}
