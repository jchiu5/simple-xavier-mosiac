﻿#pragma strict

import System.Collections.Generic;

public var damage : float = 10f;
public var chanceToHit : float = 0.8f;
public var cannotHitTag : String = "Player";

private var hitSound : AudioSource;
private var objectsHit : ArrayList;
private var startBool = false;

function Start () {
	objectsHit = new ArrayList();
    hitSound = GetComponent(AudioSource);
}

function Update () {
	
}

function OnTriggerEnter(other : Collider)
{
	objectsHit.Add(other.gameObject);
}

function OnTriggerStay(other : Collider)
{
	if(animation.isPlaying && other.tag != cannotHitTag) //&& objectsHit.Contains(other.gameObject)) //this was throwing errors never figured out why
	{
		if(objectsHit.Contains(other.gameObject) && !other.isTrigger)
		{
			//health decrement according to GDD
			if(DoesItHit(chanceToHit))
			{
				//play knock back sound here
				other.SendMessage("KnockBack", damage, SendMessageOptions.DontRequireReceiver);
				
				hitSound.Play();
				other.SendMessage("TakeDamage", damage, SendMessageOptions.DontRequireReceiver);
				Debug.Log("Hot dude: I'm hurt!" +" wait im not, im just hitting"+damage);
				objectsHit.Remove(other.gameObject);
			}
		}
	}
}

function OnTriggerExit(other : Collider)
{
	objectsHit.Remove(other.gameObject);
}

private function DoesItHit(chanceToHit : float)
{
	return (Random.value <= chanceToHit);
}