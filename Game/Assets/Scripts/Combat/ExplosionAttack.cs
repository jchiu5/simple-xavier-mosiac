﻿// Type: ExplosionAttack
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using System.Collections;
using UnityEngine;

public class ExplosionAttack : BaseAttack
{


  public override void Start()
  {
    base.Start();
  }

  public override void Update()
  {
  }

	public override void OnTriggerEnter(Collider other)
	{
		base.OnTriggerEnter(other);
		foreach(Collider item in objectsInRange)
		{
			if(!objectsHit.Contains(item.gameObject))
			{
				objectsHit.Add(item.gameObject);
				item.gameObject.SendMessage("TakeDamage", damage, SendMessageOptions.DontRequireReceiver);
				Debug.Log ("using here" + damage);

			}
		}
	}
}
