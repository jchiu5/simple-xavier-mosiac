﻿// Type: BaseAttack
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using System.Collections.Generic;
using UnityEngine;

public class BaseAttack : MonoBehaviour
{
  public List<string> cannotHitTag;
	public float damage = 10f;
  protected List<GameObject> objectsHit;
  protected List<Collider> objectsInRange;

  public virtual void Start()
  {
    this.objectsHit = new List<GameObject>();
    this.objectsInRange = new List<Collider>();
  }

  public virtual void Update()
  {
  }

  public virtual void OnTriggerEnter(Collider other)
  {
    if (this.objectsInRange.Contains(other) || this.cannotHitTag.Contains(((Component) ((Component) other).collider).tag) || ((Component) other).collider.isTrigger)
      return;
    this.objectsInRange.Add(other);
  }

  public virtual void OnTriggerExit(Collider other)
  {
    this.objectsInRange.Remove(other);
  }

  public virtual void Main()
  {
  }
}
