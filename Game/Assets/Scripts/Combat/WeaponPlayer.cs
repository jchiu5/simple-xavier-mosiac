﻿// Type: WeaponPlayer
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using System.Collections;
using UnityEngine;

public class WeaponPlayer : MonoBehaviour
{
  public float damage;
  public float chanceToHit;
  public string cannotHitTag;
  private AudioSource hitSound;
  private ArrayList objectsHit;
  private bool startBool;

  public WeaponPlayer()
		:base()
  {
    this.damage = 10f;
    this.chanceToHit = 0.8f;
    this.cannotHitTag = "Player";
  }

  public virtual void Start()
  {
    this.objectsHit = new ArrayList();
    this.hitSound = (AudioSource) ((Component) this).GetComponent(typeof (AudioSource));
  }

  public virtual void Update()
  {
  }

  public virtual void OnTriggerEnter(Collider other)
  {
    this.objectsHit.Add((object) ((Component) other).gameObject);
  }

  public virtual void OnTriggerStay(Collider other)
  {
		if(animation.isPlaying && other.tag != cannotHitTag) //&& objectsHit.Contains(other.gameObject)) //this was throwing errors never figured out why
		{
			if(objectsHit.Contains(other.gameObject) && !other.isTrigger)
			{
				//health decrement according to GDD
				if(DoesItHit(chanceToHit))
				{
					//play knock back sound here
					other.SendMessage("KnockBack", damage, SendMessageOptions.DontRequireReceiver);
					
					hitSound.Play();
					other.SendMessage("TakeDamage", damage, SendMessageOptions.DontRequireReceiver);
					Debug.Log("Hot dude: I'm hurt!" +" wait im not, im just hitting"+damage);
					objectsHit.Remove(other.gameObject);
				}
			}
		}
  }

  public virtual void OnTriggerExit(Collider other)
  {
    this.objectsHit.Remove((object) ((Component) other).gameObject);
  }

  private bool DoesItHit(float chanceToHit)
  {
    return (double) Random.value <= (double) chanceToHit;
  }

  public virtual void Main()
  {
  }
}
