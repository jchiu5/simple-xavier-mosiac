﻿#pragma strict
public var cannotHitTag : List.<String>;
public var damage : float = 10f;

protected var objectsHit : List.<GameObject>;
protected var objectsInRange : List.<Collider>;

function Start () {
    objectsHit = new List.<GameObject>();
    objectsInRange = new List.<Collider>();
}

function Update () {

}

function OnTriggerEnter(other : Collider)
{
	if(!objectsInRange.Contains(other) && !cannotHitTag.Contains(other.collider.tag) && !other.collider.isTrigger)
		objectsInRange.Add(other);
}

function OnTriggerExit(other : Collider)
{
	objectsInRange.Remove(other);
}