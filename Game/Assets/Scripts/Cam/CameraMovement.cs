﻿// Type: CameraMovement
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

[AddComponentMenu("Camera-Control/Mouse Orbit")]
public class CameraMovement : MonoBehaviour
{
  public Transform target;
  public float distance;
  public float xSpeed;
  public float ySpeed;
  public int yMinLimit;
  public int yMaxLimit;
  private float x;
  private float y;
  public float orthZoomStep;
  public int orthZoomMaxSize;
  public int orthZoomMinSize;
  private bool orthographicView;
  private Vector3 dragOrigin;

  public CameraMovement()
		:base()
  {
    this.distance = 10f;
    this.xSpeed = 250f;
    this.ySpeed = 120f;
    this.yMinLimit = -20;
    this.yMaxLimit = 80;
    this.orthZoomStep = 10f;
    this.orthZoomMaxSize = 500;
    this.orthZoomMinSize = 300;
  }

  public virtual void Start()
  {
    Screen.showCursor = (false);
    Vector3 eulerAngles = ((Component) this).transform.eulerAngles;
    this.x = (float) eulerAngles.y;
    this.y = (float) eulerAngles.x;
	// Make the rigid body not change rotation
	if (rigidbody)
		rigidbody.freezeRotation = true;
  }

  public virtual void LateUpdate()
  {
    if (this.target == null)
      return;
    this.x = this.x + (float) ((double) Input.GetAxis("Mouse X") * (double) this.xSpeed * 0.0199999995529652);
    this.y = this.y + (float) ((double) Input.GetAxis("Mouse Y") * (double) this.ySpeed * 0.0199999995529652);
    this.y = CameraMovement.ClampAngle(this.y, (float) this.yMinLimit, (float) this.yMaxLimit);
    Quaternion quaternion = Quaternion.Euler(this.y, this.x, 0.0f);
    Vector3 vector3 = (quaternion* new Vector3(0.0f, 0.0f, -this.distance))+ this.target.position;
    ((Component) this).transform.rotation =(quaternion);
    ((Component) this).transform.position = (vector3);
  }

  public static float ClampAngle(float angle, float min, float max)
  {
    if ((double) angle < -360.0)
      angle += 360f;
    if ((double) angle > 360.0)
      angle -= 360f;
    return Mathf.Clamp(angle, min, max);
  }

  public virtual void Update()
  {
    this.zoomCamera();
  }

  public virtual void zoomCamera()
  {
    if ((double) Input.GetAxis("Mouse ScrollWheel") < 0.0)
    {
      if (this.orthographicView)
      {
        if ((double) Camera.main.orthographicSize <= (double) this.orthZoomMaxSize)
          Camera.main.orthographicSize = (Camera.main.orthographicSize + this.orthZoomStep);
      }
      else if ((double) Camera.main.fieldOfView <= 102.0)
        Camera.main.fieldOfView =(Camera.main.fieldOfView + 5f);
    }
    if ((double) Input.GetAxis("Mouse ScrollWheel") <= 0.0)
      return;
    if (this.orthographicView)
    {
      if ((double) Camera.main.orthographicSize < (double) this.orthZoomMinSize)
        return;
      Camera.main.orthographicSize = (Camera.main.orthographicSize - this.orthZoomStep);
    }
    else
    {
      if ((double) Camera.main.fieldOfView < 10.0)
        return;
      Camera.main.fieldOfView =(Camera.main.fieldOfView - 5f);
    }
  }

  public virtual void Main()
  {
  }
}
