﻿#pragma strict

private var enemies : GameObject[];
private var enemiesInLOS : List.<GameObject>;
private var enemyCheck : List.<GameObject>;
private var cam : Camera;
private var planes : Plane[];
private var player : Transform;
private var target: GameObject;
public var targetNum: int; 

//public var isTargeting : boolean;

function Start () {	
	cam = Camera.main;
	planes = GeometryUtility.CalculateFrustumPlanes(cam);
	enemies = GameObject.FindGameObjectsWithTag("Enemy");	
	enemiesInLOS =  new List.<GameObject>(); 
	enemyCheck = new List.<GameObject>(); 
	//isTargeting = false;
	player = GameObject.FindGameObjectWithTag("Player").GetComponent(Transform);
	
}

function Update () {
	enemies = GameObject.FindGameObjectsWithTag("Enemy");
	enemyCheck =  new List.<GameObject>(); 	
	
	if(Input.GetKeyUp(KeyCode.LeftShift)){
	
		cam = Camera.main;
		planes = GeometryUtility.CalculateFrustumPlanes(cam);
		
		var hit : RaycastHit;
		
		for(var i = 0; i < enemies.Length; i++){
			if(GeometryUtility.TestPlanesAABB(planes,enemies[i].collider.bounds)){
			
				var rayDirection = enemies[i].transform.position - transform.position;
				if(Physics.Raycast(transform.position,rayDirection,hit)){
				
					if(hit.transform == enemies[i])
						enemyCheck.Add(enemies[i]);
				}
				enemyCheck.Add(enemies[i]);
			}
		}
		for(var j = 0; j < enemyCheck.Count; j++)
			if(!enemiesInLOS.Contains(enemyCheck[j]))
				enemiesInLOS.Add(enemyCheck[j]);
		for(var k = 0; k < enemiesInLOS.Count; k++)
			if(!enemyCheck.Contains(enemiesInLOS[k])){
				enemiesInLOS.Remove(enemiesInLOS[k]);
				k--;
			}
		Debug.Log(enemyCheck.Count);
		//Get iterator to iterate though a list, keeping a target with that value
		//Need to implement some kind of Priority Queue or something
		//asdasd
	}
			
}