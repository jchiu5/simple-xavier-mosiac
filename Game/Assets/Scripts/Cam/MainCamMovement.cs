﻿// Type: MainCamMovement
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class MainCamMovement : MonoBehaviour
{
  public float smooth;
  public float camSmooth;
  public float mouseYDamp;
  public float mouseXDamp;
  public float minAngle;
  public float maxAngle;
  public float resetCamDelta;
  private Transform player;
  private Vector3 startingPos;
  private Vector3 goalPosition;
  private Transform _transform;
  private float charHeight;
  private float camDeltaTimer;

  public MainCamMovement()
		:base()
  {
    
    this.smooth = 15f;
    this.camSmooth = 15f;
    this.mouseYDamp = 100f;
    this.mouseXDamp = 3f;
    this.minAngle = 0.05f;
    this.maxAngle = 1.95f;
    this.resetCamDelta = 2f;
  }

  public virtual void Start()
  {
    this.player = (Transform) GameObject.FindGameObjectWithTag("Player").GetComponent(typeof (Transform));
    this.startingPos = ((Component) this).transform.position- this.player.position;
    this._transform = ((Component) this).transform;
    this.charHeight = ((CharacterController) ((Component) this.player).gameObject.GetComponent(typeof (CharacterController))).height;
  }

  public virtual void Update()
  {
    if (Input.GetButtonDown("ResetCamera") || Input.GetButtonDown("xResetCamera"))
      this.startingPos = this.GetBehindVector(this.player, this.startingPos);
    this.goalPosition = this.player.position+ this.startingPos;
    this._transform.position = (Vector3.Lerp(((Component) this).transform.position, this.goalPosition, this.smooth * Time.deltaTime));
    float mouseX = GWMath.ReturnFurtherFromZero(Input.GetAxis("Mouse X"), Input.GetAxis("xMouse X"));
    float mouseY = GWMath.ReturnFurtherFromZero(Input.GetAxis("Mouse Y"), Input.GetAxis("xMouse Y"));
    Debug.DrawRay(this.player.position, this.startingPos, Color.yellow);
    if ((double) mouseX != 0.0 || (double) mouseY != 0.0)
    {
      this.camDeltaTimer = 0.0f;
      this.MoveCam(mouseX, mouseY, this.mouseXDamp, this.mouseYDamp);
    }
    else if (!Input.anyKey)
    {
      this.camDeltaTimer = this.camDeltaTimer + Time.deltaTime;
      if ((double) this.camDeltaTimer > (double) this.resetCamDelta && (double) Vector3.Angle(this.startingPos, this.GetBehindVector(this.player, this.startingPos)) > 3.0)
      {
        if (this.player.InverseTransformDirection(this.startingPos).x > 0.0)
          this.MoveCam(1f, 0.0f, this.mouseXDamp, 0.0f);
        else
          this.MoveCam(-1f, 0.0f, this.mouseXDamp, 0.0f);
      }
    }
    else
      this.camDeltaTimer = 0.0f;
    this.SmoothLook();
  }

  private void SmoothLook()
  {
		Vector3 relPlayerPosition = player.position - _transform.position + Vector3.up * charHeight / 2;
		
		Quaternion lookAtRotation = Quaternion.LookRotation(relPlayerPosition, Vector3.up);
		
		_transform.rotation = Quaternion.Lerp(transform.rotation, lookAtRotation, camSmooth * Time.deltaTime);
  }

  private void MoveCam(float mouseX, float mouseY, float mouseXDamp, float mouseYDamp)
  {
    this.startingPos = GWMath.RotateY(this.startingPos, mouseX * mouseXDamp * Time.deltaTime);
    Vector3 vector3_1 = this.startingPos;
    Vector3 vector3_2 = Vector3.Cross(this.startingPos, Vector3.up);
    this.startingPos = (Quaternion.AngleAxis(mouseY * mouseYDamp * Time.deltaTime, vector3_2)* this.startingPos);
	float num = Quaternion.Angle(Quaternion.Euler(0,1,0), Quaternion.Euler(startingPos.normalized));
    if ((double) num >= (double) this.minAngle && (double) num <= (double) this.maxAngle)
      return;
    this.startingPos = vector3_1;
  }

  private Vector3 GetBehindVector(Transform t, Vector3 v)
  {
		Vector3 behindVector = t.forward * -1;
		Vector3 twoDV = new Vector3(-v.x, 0, -v.z);
		behindVector = Quaternion.AngleAxis(Vector3.Angle(v, twoDV), -t.right) * t.forward;
		behindVector *= v.magnitude;
		return behindVector;
  }

  public virtual void Main()
  {
  }
}
