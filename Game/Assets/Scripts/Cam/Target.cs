﻿// Type: Target
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
  private GameObject[] enemies;
  private List<GameObject> enemiesInLOS;
  private List<GameObject> enemyCheck;
  private Camera cam;
  private Plane[] planes;
  private Transform player;
  private GameObject target;
  public int targetNum;

  public virtual void Start()
  {
    this.cam = Camera.main;
    this.planes = GeometryUtility.CalculateFrustumPlanes(this.cam);
    this.enemies = GameObject.FindGameObjectsWithTag("Enemy");
    this.enemiesInLOS = new List<GameObject>();
    this.enemyCheck = new List<GameObject>();
    this.player = (Transform) GameObject.FindGameObjectWithTag("Player").GetComponent(typeof (Transform));
  }

  public virtual void Update()
  {
    this.enemies = GameObject.FindGameObjectsWithTag("Enemy");
    this.enemyCheck = new List<GameObject>();
    if (!Input.GetKeyUp((KeyCode) 304))
      return;
    this.cam = Camera.main;
    this.planes = GeometryUtility.CalculateFrustumPlanes(this.cam);
    RaycastHit raycastHit = new RaycastHit();
    for (int index = 0; index < this.enemies.Length; ++index)
    {
      if (GeometryUtility.TestPlanesAABB(this.planes, this.enemies[index].collider.bounds))
      {
        // ISSUE: explicit reference operation
        if (Physics.Raycast(((Component) this).transform.position, (this.enemies[index].transform.position- ((Component) this).transform.position), out raycastHit) && ((Object) ((RaycastHit) @raycastHit).transform== (Object) this.enemies[index]))
          this.enemyCheck.Add(this.enemies[index]);
        this.enemyCheck.Add(this.enemies[index]);
      }
    }
    for (int index = 0; index < this.enemyCheck.Count; ++index)
    {
      if (!this.enemiesInLOS.Contains(this.enemyCheck[index]))
        this.enemiesInLOS.Add(this.enemyCheck[index]);
    }
    for (int index = 0; index < this.enemiesInLOS.Count; ++index)
    {
      if (!this.enemyCheck.Contains(this.enemiesInLOS[index]))
      {
        this.enemiesInLOS.Remove(this.enemiesInLOS[index]);
        --index;
      }
    }
    Debug.Log((object) this.enemyCheck.Count);
  }

  public virtual void Main()
  {
  }
}
