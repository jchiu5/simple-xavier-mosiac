﻿public var target : Transform;
public var distance = 10.0;
 
public var xSpeed = 250.0;
public var ySpeed = 120.0;
 
public var yMinLimit = -20;
public var yMaxLimit = 80;
 
private var x = 0.0;
private var y = 0.0;
 
@script AddComponentMenu("Camera-Control/Mouse Orbit")
 
function Start () {
	Screen.showCursor = false;
    var angles = transform.eulerAngles;
    x = angles.y;
    y = angles.x;
 
    // Make the rigid body not change rotation
    if (rigidbody)
       rigidbody.freezeRotation = true;
}
 
function LateUpdate () {
    if (target) {
         x += Input.GetAxis("Mouse X") * xSpeed * 0.02;
         y += Input.GetAxis("Mouse Y") * ySpeed * 0.02;
       
 
       y = ClampAngle(y, yMinLimit, yMaxLimit);
 
       var rotation = Quaternion.Euler(y, x, 0);
       var position = rotation * Vector3(0.0, 0.0, -distance) + target.position;
 
       transform.rotation = rotation;
       transform.position = position;
    }
}
 
static function ClampAngle (angle : float, min : float, max : float) {
    if (angle < -360)
       angle += 360;
    if (angle > 360)
       angle -= 360;
    return Mathf.Clamp (angle, min, max);
}

     
    public var orthZoomStep:float = 10.0f;
    public var orthZoomMaxSize :int= 500;
    public var orthZoomMinSize:int = 300;
     
    private var orthographicView = false;
    private var dragOrigin:Vector3;
     
    // Update is called once per frame
    function Update () {
        zoomCamera();
    }
     
    function zoomCamera()
    {
        // zoom out
        if (Input.GetAxis("Mouse ScrollWheel") <0)
        {
            if(orthographicView)
            {
                if (Camera.main.orthographicSize <=orthZoomMaxSize){
                    Camera.main.orthographicSize += orthZoomStep;}
            }
            else
            {
                if (Camera.main.fieldOfView<=102){
                       Camera.main.fieldOfView +=5;}
            }
        }
        // zoom in
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if(orthographicView)
            {
                if (Camera.main.orthographicSize >= orthZoomMinSize){
                     Camera.main.orthographicSize -= orthZoomStep;     
                }       
            }
            else
            {
                if (Camera.main.fieldOfView >= 10){
                    Camera.main.fieldOfView -=5;
                }
               
           }
       }
    }
