﻿#pragma strict

public var movementDamp : float = 8;

private var mainCam : Transform;
private var distance : Vector3;
private var difference : Vector3;
private var _transform : Transform;

function Start () {
	mainCam = GameObject.FindGameObjectWithTag("MainCamera").transform;
	_transform = GetComponent(Transform);
	distance = mainCam.position;
}

function LateUpdate () {
	difference = distance - mainCam.position;
	distance = mainCam.position;
	_transform.rotation = mainCam.rotation;
	_transform.position = _transform.position - difference / movementDamp;
}