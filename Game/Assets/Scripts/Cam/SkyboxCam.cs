﻿// Type: SkyboxCam
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class SkyboxCam : MonoBehaviour
{
  public float movementDamp;
  private Transform mainCam;
  private Vector3 distance;
  private Vector3 difference;
  private Transform _transform;

  public SkyboxCam()
		:base()
  {
    this.movementDamp = 8f;
  }

  public virtual void Start()
  {
    this.mainCam = GameObject.FindGameObjectWithTag("MainCamera").transform;
    this._transform = (Transform) ((Component) this).GetComponent(typeof (Transform));
    this.distance = this.mainCam.position;
  }

  public virtual void LateUpdate()
  {
    this.difference = (this.distance- this.mainCam.position);
    this.distance = this.mainCam.position;
    this._transform.rotation =(this.mainCam.rotation);
    this._transform.position = ((this._transform.position- (this.difference/ this.movementDamp)));
  }

  public virtual void Main()
  {
  }
}
