﻿// Type: FadeScreen
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class FadeScreen : MonoBehaviour
{
  public float fadeSpeed;
  private bool sceneStarting;

  public FadeScreen()
		:base()
  {
    this.fadeSpeed = 1.5f;
    this.sceneStarting = true;
  }

  public virtual void Awake()
  {
    ((Component) this).guiTexture.pixelInset = (new Rect(0.0f, 0.0f, (float) Screen.width, (float) Screen.height));
  }

  public virtual void Update()
  {
    if (!this.sceneStarting)
      return;
    this.StartScene();
  }

  public virtual void FadeToClear()
  {
    ((Component) this).guiTexture.color = (Color.Lerp(((Component) this).guiTexture.color, Color.clear, this.fadeSpeed * Time.deltaTime));
  }

  public virtual void FadeToBlack()
  {
    ((Component) this).guiTexture.color = (Color.Lerp(((Component) this).guiTexture.color, Color.black, this.fadeSpeed * Time.deltaTime));
  }

  public virtual void StartScene()
  {
    this.FadeToClear();
    if (((Component) this).guiTexture.color.a > 0.0500000007450581)
      return;
    ((Component) this).guiTexture.color = (Color.clear);
    ((Behaviour) ((Component) this).guiTexture).enabled = (false);
    this.sceneStarting = false;
  }

  public virtual void EndScene(string nextScene)
  {
    ((Behaviour) ((Component) this).guiTexture).enabled = (true);
    this.FadeToBlack();
    if (((Component) this).guiTexture.color.a < 0.949999988079071)
      return;
    Application.LoadLevel(nextScene);
  }

  public virtual void Main()
  {
  }
}
