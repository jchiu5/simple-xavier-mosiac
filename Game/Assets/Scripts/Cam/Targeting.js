#pragma strict

private var player : Transform;
private var charHeight : float;
private var targetList : TargetList;

public var target: GameObject;

function Start () 
{	
	player = GameObject.FindGameObjectWithTag("Player").GetComponent(Transform);
	charHeight = player.gameObject.GetComponent(CharacterController).height;
	targetList = new TargetList();
}

function Update () 
{	
	if(Input.GetKeyUp(KeyCode.LeftShift))
	{
		targetList.CheckVision();
		target = targetList.getTarget();
	}
		
	if(Input.GetKeyUp(KeyCode.RightShift))
	{
		(GetComponent(MainCamMovement) as MainCamMovement).enabled = true;
		target = null;
	}
	
	if(target != null)
	{
		(GetComponent(MainCamMovement) as MainCamMovement).enabled = false;
		UpdateCam();			
	}
}

function UpdateCam()
{
	var offset : Vector3 = new Vector3(player.transform.position.x - target.transform.position.x, 0.0f, 
									   player.transform.position.z - target.transform.position.z);
	transform.position = player.transform.position + (offset.normalized * 2) + Vector3(0.0f,charHeight * 2,0.0f);
	transform.position = transform.position + Vector3.Cross(offset,Vector3(0.0f,1.0f,0.0f)).normalized * 1;
	transform.LookAt(target.transform);
}

private class TargetList
{ 
	private var planes : Plane[];
	private var targetList : List.<Node>;
	
	private var cam : Camera;
	private var target: GameObject;

	function TargetList() //creates initial list containing all enemies in the scene, defaulting to not having vision of them
	{
		targetList = new List.<Node>();
		
		var enemies : GameObject[] = GameObject.FindGameObjectsWithTag("Enemy");	
		for(var i = 0; i < enemies.Length; i++)
		{
			Add(enemies[i]);
		}
	}
	
	function Add(obj : GameObject)
	{
		targetList.Add(new Node( obj, false, false));
	}
	
	function getTarget() : GameObject
	{
		if(targetList.Count <= 0)
			return null;
		var target : GameObject = targetList[0].Value;
		var priority : float = targetList[0].determinePriority(); // Priority number is Ordinal (Lower is Higher priority)
		var selector : int = 0;
		for( var j = 0; j < targetList.Count; j++)
			{
			var temp : float = targetList[j].determinePriority();
			if(temp < priority)
				{
					priority = temp;
					selector = j;
				}
			}
		if(targetList[selector].Selected == true)
			for(var k = 0; k < targetList.Count;k++)
				targetList[k].Selected = false;
		targetList[selector].Selected = true;
		target = targetList[selector].Value;
		Debug.Log(targetList[selector].Vision);
		return target;
	}
	
	function CheckVision() //checks each enemy in the list for vision
	{
		cam = Camera.main;
		planes = GeometryUtility.CalculateFrustumPlanes(cam);
		
		var hit : RaycastHit;
		
		for(var i = 0; i < targetList.Count; i++)
		{
			if(GeometryUtility.TestPlanesAABB(planes,targetList[i].Value.collider.bounds))
			{
				targetList[i].Vision = true;
				var rayDirection = targetList[i].Value.transform.position - cam.transform.position;
				if(Physics.Raycast(cam.transform.position,rayDirection,hit))
				{
					if(hit.collider == targetList[i].Value.collider)
						targetList[i].Vision = true;
					else
						targetList[i].Vision = false;
				}
			}
			else
				targetList[i].Vision = false;
		}
	}
	
	
	
	private class Node
	{
		public var Value : GameObject;
		public var Selected : boolean;
		public var Vision : boolean;
		
		public function Node(go : GameObject, sel : boolean, vis : boolean)
		{
			Value = go;
			Selected = sel;
			Vision = vis;
		}
		
		public function determinePriority() : float
		{
			var Result : float = Vector3.Distance(GameObject.FindGameObjectWithTag("Player").GetComponent(Transform).position, Value.transform.position);
			Result*= Vision ? 1 : 10; //If the target is in vision, it has 10x more priority over if it werent in vision
			Result+= Selected ? 100 : 0; 
			return Result;
		}
	}
}