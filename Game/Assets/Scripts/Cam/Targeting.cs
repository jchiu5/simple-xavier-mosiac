// Type: Targeting
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class Targeting : MonoBehaviour
{
  private Transform player;
  private float charHeight;
  private TargetList targetList;
  public GameObject target;

  public virtual void Start()
  {
    this.player = (Transform) GameObject.FindGameObjectWithTag("Player").GetComponent(typeof (Transform));
    this.charHeight = ((CharacterController) ((Component) this.player).gameObject.GetComponent(typeof (CharacterController))).height;
    this.targetList = new TargetList();
  }

  public virtual void Update()
  {
		if(Input.GetKeyUp(KeyCode.LeftShift))
		{
			targetList.CheckVision();
			target = targetList.getTarget();
		}
		
		if(Input.GetKeyUp(KeyCode.RightShift))
		{
			GetComponent<MainCamMovement>().enabled = true;
			target = null;
		}
		
		if(target != null)
		{
			GetComponent<MainCamMovement>().enabled = false;
			UpdateCam();			
		}
  }

  public virtual void UpdateCam()
  {
    Vector3 vector3_1 = new Vector3((float) (((Component) this.player).transform.position.x - this.target.transform.position.x), 0.0f, (float) (((Component) this.player).transform.position.z - this.target.transform.position.z));
    transform.position = (transform.position+ (vector3_1.normalized* 2f)+ new Vector3(0.0f, this.charHeight * 2f, 0.0f));
    Vector3 position = ((Component) this).transform.position;
    Vector3 vector3_2 = Vector3.Cross(vector3_1, new Vector3(0.0f, 1f, 0.0f));
    // ISSUE: explicit reference operation
    Vector3 vector3_3 = (((Vector3) vector3_2).normalized* 1f);
    Vector3 vector3_4 = (position+ vector3_3);
    transform.position = (vector3_4);
    ((Component) this).transform.LookAt(this.target.transform);
  }

  public virtual void Main()
  {
  }
}
