﻿#pragma strict

public var smooth : float = 15f;
public var camSmooth : float = 15f;
public var mouseYDamp : float = 100f;
public var mouseXDamp : float = 3f;
public var minAngle : float = 0.05f;
public var maxAngle : float = 1.95f;

public var resetCamDelta : float = 2f;

private var player : Transform;
private var startingPos : Vector3;
private var goalPosition : Vector3;
private var _transform : Transform;
private var charHeight : float;
private var camDeltaTimer : float = 0f;

function Start () {	
	player = GameObject.FindGameObjectWithTag("Player").GetComponent(Transform);
	startingPos = transform.position - player.position;
	_transform = transform;
	charHeight = player.gameObject.GetComponent(CharacterController).height;
}

function Update () {
	if(Input.GetButtonDown("ResetCamera") || Input.GetButtonDown("xResetCamera"))
	{
		//reset camera
		startingPos = GetBehindVector(player,startingPos);
	}
	goalPosition = player.position + startingPos;
	
	_transform.position = Vector3.Lerp(transform.position, goalPosition, smooth * Time.deltaTime);;
	
	var mouseX : float = GWMath.ReturnFurtherFromZero(Input.GetAxis("Mouse X"), Input.GetAxis("xMouse X"));
	var mouseY : float = GWMath.ReturnFurtherFromZero(Input.GetAxis("Mouse Y"), Input.GetAxis("xMouse Y"));
	
	Debug.DrawRay(player.position, startingPos, Color.yellow);
	
	if(mouseX != 0 || mouseY != 0)
	{
		camDeltaTimer = 0f;
		MoveCam(mouseX, mouseY, mouseXDamp, mouseYDamp);
	}
	else if(!Input.anyKey && Input.GetAxis("xVertical") == 0 && Input.GetAxis("xHorizontal") == 0)
	{
		camDeltaTimer += Time.deltaTime;
		if(camDeltaTimer > resetCamDelta)
		{
			var behindVector : Vector3 = GetBehindVector(player,startingPos);
			if(Vector3.Angle(startingPos,behindVector) > 3f)
			{
				if(player.InverseTransformDirection(startingPos).x > 0)
				{
					MoveCam(1,0,mouseXDamp, 0);
				}
				else
					MoveCam(-1,0,mouseXDamp,0);
			}
		}
	}
	else
	{
		camDeltaTimer = 0f;
	}
	
	SmoothLook();
}

private function SmoothLook()
{
	var relPlayerPosition = player.position - _transform.position + Vector3.up * charHeight / 2;

    var lookAtRotation = Quaternion.LookRotation(relPlayerPosition, Vector3.up);
    
    _transform.rotation = Quaternion.Lerp(transform.rotation, lookAtRotation, camSmooth * Time.deltaTime);
}

private function MoveCam(mouseX : float, mouseY : float, mouseXDamp : float, mouseYDamp : float)
{
	startingPos = GWMath.RotateY(startingPos, mouseX * mouseXDamp * Time.deltaTime);
	var prevStartingPos = startingPos;
	var crossRight = Vector3.Cross(startingPos, Vector3.up);
	var yRotation : Quaternion = Quaternion.AngleAxis(mouseY * mouseYDamp * Time.deltaTime, crossRight);
	startingPos = yRotation * startingPos;
	var angle : float = Quaternion.Angle(Quaternion.Euler(0,1,0), Quaternion.Euler(startingPos.normalized));
	if(angle < minAngle || angle > maxAngle)
	{
		startingPos = prevStartingPos;
	}
}

//returns the vector position that is behind the given transform
//ignores the y value of the vector
private function GetBehindVector(t : Transform, v : Vector3)
{
	var behindVector = t.forward * -1;
	var twoDV = new Vector3(-v.x, 0, -v.z);
	behindVector = Quaternion.AngleAxis(Vector3.Angle(v, twoDV), -t.right) * t.forward;
	behindVector *= v.magnitude;
	return behindVector;
}