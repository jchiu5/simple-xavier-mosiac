﻿// Type: TargetList
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using System.Collections.Generic;
using UnityEngine;

internal class TargetList
{
  private Plane[] planes;
  private List<TargetList.Node> targetList;
  private Camera cam;
  private GameObject target;

  public TargetList()
  {
    this.targetList = new List<TargetList.Node>();
    GameObject[] gameObjectsWithTag = GameObject.FindGameObjectsWithTag("Enemy");
    for (int index = 0; index < gameObjectsWithTag.Length; ++index)
      this.Add(gameObjectsWithTag[index]);
  }

  public virtual void Add(GameObject obj)
  {
    this.targetList.Add(new TargetList.Node(obj, false, false));
  }

  public virtual GameObject getTarget()
  {
    if (this.targetList.Count <= 0)
      return (GameObject) null;
    GameObject gameObject1 = this.targetList[0].Value;
    float num1 = this.targetList[0].determinePriority();
    int index1 = 0;
    for (int index2 = 0; index2 < this.targetList.Count; ++index2)
    {
      float num2 = this.targetList[index2].determinePriority();
      if ((double) num2 < (double) num1)
      {
        num1 = num2;
        index1 = index2;
      }
    }
    if (this.targetList[index1].Selected)
    {
      for (int index2 = 0; index2 < this.targetList.Count; ++index2)
        this.targetList[index2].Selected = false;
    }
    this.targetList[index1].Selected = true;
    GameObject gameObject2 = this.targetList[index1].Value;
		Debug.Log(targetList[index1].Vision);
    return gameObject2;
  }

  public virtual void CheckVision()
  {
		cam = Camera.main;
		planes = GeometryUtility.CalculateFrustumPlanes(cam);
		
		RaycastHit hit = new RaycastHit();
		
		for(int i = 0; i < targetList.Count; ++i)
		{
			if(GeometryUtility.TestPlanesAABB(planes,targetList[i].Value.collider.bounds))
			{
				targetList[i].Vision = true;
				var rayDirection = targetList[i].Value.transform.position - cam.transform.position;
				if(Physics.Raycast(cam.transform.position,rayDirection,out hit))
				{
					if(hit.collider == targetList[i].Value.collider)
						targetList[i].Vision = true;
					else
						targetList[i].Vision = false;
				}
			}
			else
				targetList[i].Vision = false;
		}
  }

  private class Node
  {
    public GameObject Value;
    public bool Selected;
    public bool Vision;

    public Node(GameObject go, bool sel, bool vis)
    {
      this.Value = go;
      this.Selected = sel;
      this.Vision = vis;
    }

    public virtual float determinePriority()
    {
      return (float) ((double) Vector3.Distance(((Transform) GameObject.FindGameObjectWithTag("Player").GetComponent(typeof (Transform))).position, this.Value.transform.position) * (!this.Vision ? 10.0 : 1.0) + (!this.Selected ? 0.0 : 100.0));
    }
  }
}
