﻿// Type: MouseLookJS
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class MouseLookJS : MonoBehaviour
{
  public float xSensitivity;
  public float ySensitivity;
  public float minimumY;
  public float maximumY;
  public bool invertedY;
  private float rotationY;
  private float rotationX;

  public MouseLookJS()
		:base()
  {
    this.xSensitivity = 15f;
    this.ySensitivity = 15f;
    this.minimumY = -60f;
    this.maximumY = 60f;
    this.invertedY = true;
  }

  public virtual void Start()
  {
    if (this.invertedY)
      this.rotationY = (float) ((Component) this).transform.eulerAngles.x;
    else
      this.rotationY = (float) -((Component) this).transform.eulerAngles.x;
  }

  public virtual void Update()
  {
    this.rotationX = (float) (((Component) this).transform.localEulerAngles.y + (double) Input.GetAxis("Mouse X") * (double) this.xSensitivity);
    this.rotationY = this.rotationY + Input.GetAxis("Mouse Y") * this.ySensitivity;
    this.rotationY = Mathf.Clamp(this.rotationY, this.minimumY, this.maximumY);
    if (!this.invertedY)
      ((Component) this).transform.localEulerAngles =(new Vector3(-this.rotationY, this.rotationX, 0.0f));
    else
      ((Component) this).transform.localEulerAngles =(new Vector3(this.rotationY, this.rotationX, 0.0f));
  }

  public virtual void Main()
  {
  }
}
