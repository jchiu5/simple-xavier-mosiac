﻿#pragma strict

public var xSensitivity : float = 15f;
public var ySensitivity : float = 15f;
public var minimumY : float = -60f;
public var maximumY : float = 60f;
public var invertedY = true;

private var rotationY : float;
private var rotationX : float = 0f;

function Start () {
	if(invertedY)
		rotationY = transform.eulerAngles.x;
	else
		rotationY = -transform.eulerAngles.x;
}

function Update () {
	rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * xSensitivity;
			
	rotationY += Input.GetAxis("Mouse Y") * ySensitivity;
	rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
	
	if(!invertedY)
		transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
	else	
		transform.localEulerAngles = new Vector3(rotationY, rotationX, 0);
}