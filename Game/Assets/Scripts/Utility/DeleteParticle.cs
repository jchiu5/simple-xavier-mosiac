﻿// Type: DeleteParticle
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class DeleteParticle : MonoBehaviour
{
  private ParticleSystem ps;

  public virtual void Start()
  {
    this.ps = (ParticleSystem) ((Component) this).GetComponent(typeof (ParticleSystem));
  }

  public virtual void Update()
  {
	if(!ps.IsAlive())
	{
		Destroy(gameObject);
	}
  }

  public virtual void Main()
  {
  }
}
