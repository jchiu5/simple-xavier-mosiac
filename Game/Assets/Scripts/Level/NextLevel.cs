// Type: NextLevel
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class NextLevel : MonoBehaviour
{
  public string nextScene;

  public virtual void OnTriggerEnter(Collider other)
  {
    if (!(((Component) other).tag == "Player"))
      return;
    Application.LoadLevel(this.nextScene);
  }

  public virtual void Main()
  {
  }
}
