﻿#pragma strict

public var nextScene : String;

private var fadeOut : FadeScreen;
private var sceneEnding : boolean;

function OnTriggerEnter(other : Collider)
{
	if(other.tag == "Player")
	{
		fadeOut= GameObject.FindGameObjectWithTag("Fader").GetComponent(FadeScreen); //obtain FadeScreen script
		sceneEnding = true; //scene is ending once player touches portal
	}	
}

function Update ()
{
    if(sceneEnding)
        fadeOut.EndScene(nextScene);//scene is ending, call the EndScene function to fade out
}