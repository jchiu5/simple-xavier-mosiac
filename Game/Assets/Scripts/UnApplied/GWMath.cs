﻿// Type: GWMath
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class GWMath : MonoBehaviour
{
  public static Vector3 RotateX(Vector3 v, float angle)
  {
    float num1 = Mathf.Sin(angle);
    float num2 = Mathf.Cos(angle);
    float num3 = (float) v.y;
    float num4 = (float) v.z;
		v.y = (num2 * num3) - (num1 * num4);
		v.z = (num2 * num4) + (num1 * num3);
    return v;
  }

  public static Vector3 RotateY(Vector3 v, float angle)
  {
    float num1 = Mathf.Sin(angle);
    float num2 = Mathf.Cos(angle);
    float num3 = (float) v.x;
    float num4 = (float) v.z;
    v.x = ( num2 *  num3) +  (num1 * num4);
    v.z = ( num2 * num4) - (num1 * num3);
    return v;
  }

  public static Vector3 RotateZ(Vector3 v, float angle)
  {
    float num1 = Mathf.Sin(angle);
    float num2 = Mathf.Cos(angle);
    float num3 = (float) v.x;
    float num4 = (float) v.y;
    v.x = ( num2 *  num3 -  num1 *  num4);
    v.y = ( num2 *  num4 +  num1 *  num3);
    return v;
  }

  public static float ReturnFurtherFromZero(float a, float b)
  {
    if ( Mathf.Max(Mathf.Abs(a), Mathf.Abs(b)) ==  Mathf.Abs(a))
      return a;
    else
      return b;
  }

  public virtual void Main()
  {
  }
}
