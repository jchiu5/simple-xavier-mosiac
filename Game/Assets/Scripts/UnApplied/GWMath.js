﻿#pragma strict

public static function RotateX(v : Vector3, angle : float)
{
	var sin : float = Mathf.Sin(angle);
	var cos : float = Mathf.Cos(angle);
	
	var ty : float = v.y;
	var tz : float = v.z;
	
	v.y = (cos * ty) - (sin * tz);
	v.z = (cos * tz) + (sin * ty);
	
	return v;
}

public static function RotateY(v : Vector3, angle : float)
{
	var sin : float = Mathf.Sin(angle);
	var cos : float = Mathf.Cos(angle);
	
	var tx : float = v.x;
	var tz : float = v.z;
	
	v.x = (cos * tx) + (sin * tz);
	v.z = (cos * tz) - (sin * tx);
	
	return v;
}

public static function RotateZ(v : Vector3, angle : float)
{
	var sin : float = Mathf.Sin( angle );
    var cos : float = Mathf.Cos( angle );

    var tx : float = v.x;
    var ty : float = v.y;

    v.x = (cos * tx) - (sin * ty);
    v.y = (cos * ty) + (sin * tx);
    
    return v;
}

public static function ReturnFurtherFromZero(a : float, b : float)
{
	var absA = Mathf.Abs(a);
	var absB = Mathf.Abs(b);
	if(Mathf.Max(absA, absB) == Mathf.Abs(a))
	{
		return a;
	}
	else
		return b;
}