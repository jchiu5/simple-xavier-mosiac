﻿#pragma strict

private var anim : Animator;
private var nav : NavMeshAgent;

function Start () {
	anim = GetComponent(Animator);
	nav = GetComponent(NavMeshAgent);
}

function OnAnimatorMove()
{
	var speed = Vector3.Project(nav.velocity, transform.forward).magnitude;
	anim.SetFloat("Speed", speed);
}