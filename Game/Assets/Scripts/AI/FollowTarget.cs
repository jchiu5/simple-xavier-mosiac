﻿// Type: FollowTarget
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

[RequireComponent(typeof (NavMeshAgent))]
public class FollowTarget : MonoBehaviour
{
  public Transform target;
  private NavMeshAgent _NavMeshAgent;
  public virtual void Start()
  {
    this._NavMeshAgent = GetComponent<NavMeshAgent>();
    if (this.target != null)
      return;
    this.target = GameObject.FindGameObjectWithTag("Player").transform;
  }

  public virtual void Update()
  {
    this._NavMeshAgent.SetDestination(this.target.position);
  }

  public virtual void Main()
  {
  }
}
