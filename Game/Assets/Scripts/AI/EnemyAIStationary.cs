﻿// Type: EnemyAIStationary
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using System.Collections;
using UnityEngine;

public class EnemyAIStationary : MonoBehaviour
{
  public float FOV;
  public float chaseWaitTime;
  public float chaseSpeed;
  public float walkSpeed;
  public Transform eyePos;
  public bool isInSight;
  protected Vector3 destination;
  protected Vector3 startPos;
  protected Vector3 offsetPlayerPos;
  protected Transform player;
  protected float playerHeight;
  protected NavMeshAgent nav;
  protected float waitTimer;
  protected float currentSpeed;
  protected bool returnHome;
  protected AIState currentState;

  public EnemyAIStationary()
		:base()
  {
    this.FOV = 180f;
    this.chaseWaitTime = 4.5f;
    this.chaseSpeed = 4f;
    this.walkSpeed = 1.5f;
    this.currentState = AIState.Neutral;
  }

  public virtual void Start()
  {
    this.player = GameObject.FindGameObjectWithTag("Player").transform;
    this.playerHeight = ((CharacterController) ((Component) this.player).gameObject.GetComponent(typeof (CharacterController))).height / 2f;
    this.FOV = this.FOV / 2f;
    this.nav = (NavMeshAgent) ((Component) this).GetComponent(typeof (NavMeshAgent));
    this.startPos = ((Component) this).transform.position;
    this.destination = this.startPos;
    this.currentSpeed = this.walkSpeed;
    ((Component) this).transform.position = (((Component) this).transform.position + (Vector3.one * 0.0001f));
  }

  public virtual void Update()
  {
    this.isInSight = this.PlayerInSight();
    if (this.currentState != AIState.Returning && this.isInSight)
      this.currentState = AIState.Chasing;
    this.StateMoveSpeed(this.currentState);
    this.StateMovement(this.currentSpeed, this.destination, this.isInSight);
  }

  protected virtual bool PlayerInSight()
  {
    this.offsetPlayerPos = (this.player.position + (Vector3.up * this.playerHeight));
		RaycastHit hit = new RaycastHit();
		Vector3 rayDirection = (this.offsetPlayerPos - this.eyePos.position);
		Debug.DrawRay(this.eyePos.position, rayDirection);
	if(Physics.Raycast(eyePos.position, rayDirection, out hit))
	{
		return(hit.transform == player && FOV > Vector3.Angle(transform.forward, rayDirection));
	}
	else
		return false;
  }

  protected virtual bool MoveTo(float speed, Vector3 destination)
  {
    bool flag = false;
    this.nav.speed = (speed);
    if ((double) this.nav.remainingDistance < (double) this.nav.stoppingDistance)
      flag = true;
    this.nav.SetDestination(destination);
    return flag;
  }

  public virtual void TakeDamage(float damage)
  {
    if (this.currentState != AIState.Returning)
    {
      this.currentState = AIState.Chasing;
      this.destination = this.player.position;
    }
    if (!((Behaviour) this).enabled)
      return;
    this.StartCoroutine_Auto(this.KnockBack(0.6f, damage * 1.5f));
  }

	private IEnumerator KnockBack(float time, float damage)
	{
		rigidbody.AddForce((transform.position - player.position).normalized * damage, ForceMode.Impulse);
		NavMeshAgent navMeshAgent = GetComponent<NavMeshAgent>();
		float originalSpeed = navMeshAgent.speed;
		while(time > 0)
		{
			time -= Time.deltaTime;
			navMeshAgent.speed = 0;
			yield return null;
		}
		navMeshAgent.speed = originalSpeed;
	}

  public virtual void ReturnHome(bool goingHome)
  {
    if (goingHome)
      this.currentState = AIState.Returning;
    else
      this.currentState = AIState.Neutral;
  }

  protected virtual void StateMoveSpeed(AIState givenState)
  {
    if (givenState == AIState.Returning)
    {
      this.destination = this.startPos;
      this.currentSpeed = this.walkSpeed;
    }
    else if (givenState == AIState.Chasing)
    {
      this.currentSpeed = this.chaseSpeed;
      if (!this.isInSight)
        return;
      this.destination = this.player.position;
    }
    else
    {
      if (givenState != AIState.Neutral)
        return;
      this.destination = this.startPos;
      this.currentSpeed = this.walkSpeed;
    }
  }

  protected virtual void StateMovement(float speed, Vector3 destination, bool isInSight)
  {
    if (this.MoveTo(speed, destination) && !isInSight)
    {
      if (this.currentState == AIState.Returning)
        this.currentState = AIState.Neutral;
      this.waitTimer = this.waitTimer + Time.deltaTime;
      if (this.waitTimer < this.chaseWaitTime)
        return;
      this.currentState = AIState.Neutral;
      this.waitTimer = 0.0f;
    }
    else
      this.waitTimer = 0.0f;
  }

  public virtual void Main()
  {
  }
}
