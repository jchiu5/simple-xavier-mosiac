﻿// Type: EnemyAIWander
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class EnemyAIWander : EnemyAIStationary
{
  public float minPatrolWaitTime;
  public float maxPatrolWaitTime;
  public float minPatrolDist;
  public float maxPatrolDist;
  public float patrolSpeed;
  private bool patrolling;
  private float patrolTimer;
  private float patrolWaitTime;

  public EnemyAIWander()
  {
    this.minPatrolWaitTime = 1f;
    this.maxPatrolWaitTime = 3f;
    this.minPatrolDist = 2f;
    this.maxPatrolDist = 5f;
    this.patrolSpeed = 1.9f;
    this.patrolling = true;
  }

  public override void Start()
  {
    base.Start();
    this.currentState = AIState.Neutral;
    this.destination = this.GetPatrolRoute();
    this.patrolWaitTime = Random.Range(this.minPatrolWaitTime, this.maxPatrolWaitTime);
  }

  public override void Update()
  {
    this.isInSight = this.PlayerInSight();
    if (this.currentState != AIState.Returning && this.isInSight)
      this.currentState = AIState.Chasing;
    this.StateMoveSpeed(this.currentState);
    this.StateMovement(this.currentSpeed, this.destination, this.isInSight);
  }

  private Vector3 GetPatrolRoute()
  {
		//return a randomVector3 inside box collider of parent
	Vector3 randDestination = new Vector3(1,0,1);
	randDestination = randDestination * Random.Range(minPatrolDist, maxPatrolDist);
	randDestination = Quaternion.AngleAxis(Random.Range(0,360),Vector3.up) * randDestination;
	randDestination = randDestination + transform.position;
	return randDestination;
  }

  public virtual void OnDrawGizmosSelected()
  {
    Gizmos.color = (Color.blue);
    Gizmos.DrawSphere(this.destination, 1f);
  }

  protected override void StateMoveSpeed(AIState givenState)
  {
    if (givenState == AIState.Returning)
    {
      this.destination = this.startPos;
      this.currentSpeed = this.walkSpeed;
    }
    else if (givenState == AIState.Chasing)
    {
      this.currentSpeed = this.chaseSpeed;
      if (!this.isInSight)
        return;
      this.destination = this.player.position;
    }
    else
    {
      if (givenState != AIState.Neutral)
        return;
      this.currentSpeed = this.patrolSpeed;
    }
  }

  protected override void StateMovement(float speed, Vector3 destination, bool isInSight)
  {
    bool flag = this.MoveTo(speed, destination);
    if (flag)
    {
      if (this.currentState == AIState.Returning)
        this.currentState = AIState.Neutral;
      if (!isInSight)
      {
        if (this.currentState == AIState.Neutral)
        {
          this.waitTimer = 0.0f;
          this.patrolTimer = this.patrolTimer + Time.deltaTime;
          if ((double) this.patrolTimer >= (double) this.patrolWaitTime)
          {
            this.destination = this.GetPatrolRoute();
            this.patrolTimer = 0.0f;
            this.patrolWaitTime = Random.Range(this.minPatrolWaitTime, this.maxPatrolWaitTime);
          }
        }
        else
        {
          this.patrolTimer = 0.0f;
          this.waitTimer = this.waitTimer + Time.deltaTime;
          if ((double) this.waitTimer >= (double) this.chaseWaitTime)
          {
            this.currentState = AIState.Neutral;
            this.waitTimer = 0.0f;
          }
        }
      }
    }
    if (!isInSight || flag)
      return;
    this.patrolTimer = 0.0f;
    this.waitTimer = 0.0f;
  }
}
