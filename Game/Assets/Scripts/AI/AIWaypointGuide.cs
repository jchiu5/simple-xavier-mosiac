﻿// Type: AIWaypointGuide
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

[RequireComponent(typeof (NavMeshAgent))]
public class AIWaypointGuide : MonoBehaviour
{
  public Transform[] waypoints;
	public float playerRange = 3.75f;
  private float originalSpeed;
  private Transform _transform;
  private Transform player;
  private int location;
  private NavMeshAgent _NavMeshAgent;

  public virtual void Start()
  {
    this.player = (Transform) GameObject.FindGameObjectWithTag("Player").GetComponent(typeof (Transform));
    this._NavMeshAgent =GetComponent<NavMeshAgent>();
		this._transform = transform;
    this.location = 0;
    this.originalSpeed = this._NavMeshAgent.speed;
  }

  public virtual void Update()
  {
    this._NavMeshAgent.speed = (this.originalSpeed);
    this._NavMeshAgent.destination = (this.waypoints[this.location].position);
    Vector3 vector3_1 = (this._transform.position - this.waypoints[this.location].position);
	if((_transform.position - waypoints[location].position).magnitude < _NavMeshAgent.stoppingDistance)
	{
		if((_transform.position - player.position).magnitude > playerRange)
		{
			//looks pretty weird
			//_transform.rotation = Quaternion.Lerp(_transform.rotation, Quaternion.LookRotation(player.position - _transform.position), Time.deltaTime);
		}
		else
		{
			if(location != waypoints.Length)
				location++;
		}
	}
  }

  public virtual void Main()
  {
  }
}
