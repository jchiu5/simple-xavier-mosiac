﻿#pragma strict

public class EnemyAttack extends Attack {
public var attackRange : float = 10;
public var gizmoColor : Color;

private var enemyAI : EnemyAIStationary;
private var player : Transform;

override function Start () {
	super.Start();
	enemyAI = GetComponent(EnemyAIStationary);
	player = GameObject.FindGameObjectWithTag("Player").transform;
}

override function Update () {
	//if player is in sight and within range attack
	if(enemyAI.isInSight && Vector3.Distance(transform.position, player.position) < attackRange)
	{
		BeginAttack();
	}
}

function OnDrawGizmosSelected() {
	Gizmos.color = gizmoColor;
	Gizmos.DrawSphere(transform.position, attackRange);
}
}