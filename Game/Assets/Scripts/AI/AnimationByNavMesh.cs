﻿// Type: AnimationByNavMesh
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class AnimationByNavMesh : MonoBehaviour
{
  private Animator anim;
  private NavMeshAgent nav;
	private float speed;

  public virtual void Start()
  {
    this.anim = GetComponent<Animator>();
    this.nav = GetComponent<NavMeshAgent>();
  }

  public virtual void OnAnimatorMove()
  {
	speed = Vector3.Project(nav.velocity, transform.forward).magnitude;
	anim.SetFloat("Speed", speed);
  }

  public virtual void Main()
  {
  }
}
