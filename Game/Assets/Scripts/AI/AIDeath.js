﻿#pragma strict

private var anim : Animator;
private var justDied = false;

function Start () {
	anim = GetComponent(Animator);
}

function Update () {
	
}

public function Die()
{
	GetComponent(EnemyAttack).enabled = false;
	GetComponent(EnemyAIStationary).enabled = false;
	anim.SetTrigger("isDead");
	anim.SetFloat("Speed", 0.0f);
	collider.enabled = false;
	GetComponent(NavMeshAgent).enabled = false;
	GetComponent(Health).enabled = false;
	rigidbody.velocity = Vector3.zero;
}