﻿#pragma strict

class EnemyAIWander extends EnemyAIStationary {

public var minPatrolWaitTime : float = 1f;
public var maxPatrolWaitTime : float = 3f;
public var minPatrolDist : float = 2f;
public var maxPatrolDist : float = 5f;
public var patrolSpeed : float = 1.9f;

private var patrolling : boolean = true;
private var patrolTimer : float = 0f;
private var patrolWaitTime : float = 0f;

function Start()
{
	super.Start();
	currentState = AIState.Neutral;
	destination = GetPatrolRoute();
	patrolWaitTime = Random.Range(minPatrolWaitTime, maxPatrolWaitTime);
}

override function Update () {
	isInSight = PlayerInSight();
	if(currentState != AIState.Returning)
	{
		if(isInSight)
		{
			//chase player
			currentState = AIState.Chasing;
		}
	}
	
	StateMoveSpeed(currentState);
	StateMovement(currentSpeed,destination,isInSight);
}

private function GetPatrolRoute()
{
	//return a randomVector3 inside box collider of parent
	var randDestination = new Vector3(1,0,1);
	randDestination = randDestination * Random.Range(minPatrolDist, maxPatrolDist);
	randDestination = Quaternion.AngleAxis(Random.Range(0,360),Vector3.up) * randDestination;
	randDestination = randDestination + transform.position;
	return randDestination;
}

function OnDrawGizmosSelected() {
	Gizmos.color = Color.blue;
	Gizmos.DrawSphere(destination, 1f);
}

protected override function StateMoveSpeed(givenState : AIState)
{
	if(givenState == AIState.Returning)
	{
		destination = startPos;
		currentSpeed = walkSpeed;
	}
	else if(givenState == AIState.Chasing)
	{
		currentSpeed = chaseSpeed;
		if(isInSight)
		{
			destination = player.position;
		}
	}
	else if(givenState == AIState.Neutral)
	{
		currentSpeed = patrolSpeed;
	}
}

protected override function StateMovement(speed : float, destination : Vector3, isInSight : boolean)
{
	var shouldStop : boolean = MoveTo(speed, destination);
	if(shouldStop)
	{
		if(currentState == AIState.Returning)
		{
			currentState = AIState.Neutral;
		}
		if(!isInSight)
		{
			if(currentState == AIState.Neutral)
			{
				waitTimer = 0f;
				patrolTimer += Time.deltaTime;
				if(patrolTimer >= patrolWaitTime)
				{
					this.destination = GetPatrolRoute();				
					patrolTimer = 0f;
					patrolWaitTime = Random.Range(minPatrolWaitTime, maxPatrolWaitTime);
				}
			}
			else
			{
				patrolTimer = 0f;
				waitTimer += Time.deltaTime;
				if(waitTimer >= chaseWaitTime)
				{
					currentState = AIState.Neutral;
					waitTimer = 0f;
				}
			}
		}
	}
	if(isInSight && !shouldStop)
	{
		patrolTimer = 0f;
		waitTimer = 0f;
	}
}
}