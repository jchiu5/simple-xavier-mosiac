﻿// Type: AIDeath
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class AIDeath : MonoBehaviour
{
  private Animator anim;
  private bool justDied;

  public virtual void Start()
  {
    this.anim = GetComponent<Animator>();
  }

  public virtual void Update()
  {
  }

  public virtual void Die()
  {
    GetComponent<EnemyAttack>().enabled =(false);
    GetComponent<EnemyAIStationary>().enabled =(false);
    this.anim.SetTrigger("isDead");
    this.anim.SetFloat("Speed", 0.0f);
    collider.enabled =(false);
    GetComponent<NavMeshAgent>().enabled =(false);
    GetComponent<Health>().enabled =(false);
    rigidbody.velocity = (Vector3.zero);
  }

  public virtual void Main()
  {
  }
}
