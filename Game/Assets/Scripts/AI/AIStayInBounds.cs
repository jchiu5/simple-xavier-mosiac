﻿// Type: AIStayInBounds
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class AIStayInBounds : MonoBehaviour
{
  private EnemyAIStationary enemyAI;

  public virtual void Start()
  {
  }

  public virtual void Update()
  {
  }

  public virtual void OnTriggerExit(Collider other)
  {
    if (!other.transform.IsChildOf(transform))
	{
	return;
	}
    other.SendMessage("ReturnHome", true, (SendMessageOptions) 1);
  }

  public virtual void Main()
  {
  }
}
