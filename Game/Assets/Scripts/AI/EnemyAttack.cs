﻿// Type: EnemyAttack
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class EnemyAttack : Attack
{
  public float attackRange;
  public Color gizmoColor;
  private EnemyAIStationary enemyAI;
  private Transform player;

  public EnemyAttack()
  {
    this.attackRange = 10f;
  }

  public override void Start()
  {
    base.Start();
    this.enemyAI = GetComponent<EnemyAIStationary>();
		this.player = GameObject.FindGameObjectWithTag("Player").transform;
  }

  public override void Update()
  {
    if (!this.enemyAI.isInSight || (double) Vector3.Distance(((Component) this).transform.position, this.player.position) >= (double) this.attackRange)
      return;
    this.BeginAttack();
  }

  public virtual void OnDrawGizmosSelected()
  {
    Gizmos.color = (this.gizmoColor);
    Gizmos.DrawSphere(((Component) this).transform.position, this.attackRange);
  }
}
