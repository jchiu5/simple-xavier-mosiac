﻿#pragma strict

public var target : Transform;

private var _NavMeshAgent : NavMeshAgent;

@script RequireComponent(NavMeshAgent)
function Start () {
	_NavMeshAgent = GetComponent(NavMeshAgent);
	if(target == null)
	{
		target = GameObject.FindGameObjectWithTag("Player").transform;
	}
}

function Update () {
	_NavMeshAgent.SetDestination(target.position);
}