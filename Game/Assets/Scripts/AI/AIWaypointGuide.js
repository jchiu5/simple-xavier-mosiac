﻿#pragma strict

public var waypoints : Transform[];
public var playerRange : float = 3.75f;

private var originalSpeed : float;
private var _transform : Transform;
private var player : Transform;
private var location : int;
private var _NavMeshAgent : NavMeshAgent;

@script RequireComponent(NavMeshAgent)
function Start () {
	player = GameObject.FindGameObjectWithTag("Player").GetComponent(Transform);
	_NavMeshAgent = GetComponent(NavMeshAgent);
	_transform = transform;
	location = 0;
	originalSpeed = _NavMeshAgent.speed;
}

function Update () {
	//check if you are at the location
	//if not walk there
	//if there check if player is close by
	//if not wait and face him
	_NavMeshAgent.speed = originalSpeed;
	_NavMeshAgent.destination = waypoints[location].position;
	if(Mathf.Abs((_transform.position - waypoints[location].position).magnitude) < _NavMeshAgent.stoppingDistance)
	{
		if(Mathf.Abs((_transform.position - player.position).magnitude) > playerRange)
		{
			//looks pretty weird
			//_transform.rotation = Quaternion.Lerp(_transform.rotation, Quaternion.LookRotation(player.position - _transform.position), Time.deltaTime);
		}
		else
		{
			if(location != waypoints.Length)
				location++;
		}
	}
}