﻿#pragma strict

public var FOV : float = 180f;
public var chaseWaitTime : float = 4.5f;
public var chaseSpeed : float = 4f;
public var walkSpeed : float = 1.5f;
public var eyePos : Transform;
public var isInSight : boolean;

protected var destination : Vector3;
protected var startPos : Vector3;
protected var offsetPlayerPos : Vector3;
protected var player : Transform;
protected var playerHeight : float;
protected var nav : NavMeshAgent;
protected var waitTimer : float = 0f;
protected var currentSpeed : float;
protected var returnHome : boolean = false;
protected var currentState : AIState = AIState.Neutral;

public enum AIState {Neutral, Chasing, Returning};

function Start () {
	player = GameObject.FindGameObjectWithTag("Player").transform;
	playerHeight = player.gameObject.GetComponent(CharacterController).height / 2;
	FOV = FOV/2;
	nav = GetComponent(NavMeshAgent);
	startPos = transform.position;
	destination = startPos;
	currentSpeed = walkSpeed;
	transform.position = transform.position + Vector3.one * 0.0001f;	//set this to collide with stuff
}

function Update () {
	isInSight = PlayerInSight();
	if(currentState != AIState.Returning)
	{
		if(isInSight)
		{
			//chase player
			currentState = AIState.Chasing;
		}
	}
	
	StateMoveSpeed(currentState);
	StateMovement(currentSpeed,destination,isInSight);
}

protected function PlayerInSight()
{
	//needs to look at the hips of the player
	offsetPlayerPos = player.position + Vector3.up * playerHeight;
	var hit : RaycastHit;
	var rayDirection = offsetPlayerPos - eyePos.position;
	Debug.DrawRay(eyePos.position, rayDirection);
	
	if(Physics.Raycast(eyePos.position, rayDirection, hit))
	{
		return(hit.transform == player && FOV > Vector3.Angle(transform.forward, rayDirection));
	}
	else
		return false;
}

protected function MoveTo (speed : float, destination : Vector3)
{
	var shouldStop = false;
	nav.speed = speed;
    
    // If near the next waypoint or there is no destination...
    if(nav.remainingDistance < nav.stoppingDistance)
    {
        shouldStop = true;
    }
    nav.SetDestination(destination);
    
    return shouldStop;
}

function TakeDamage (damage : float)
{
	if(currentState != AIState.Returning)
	{
		currentState = AIState.Chasing;
		destination = player.position;
	}
	if(this.enabled)
	{
		KnockBack(0.6f, damage * 1.5f);
	}
}

private function KnockBack(time : float, damage : float)
{
	rigidbody.AddForce((transform.position - player.position).normalized * damage, ForceMode.Impulse);
	var navMeshAgent = GetComponent(NavMeshAgent);
	var originalSpeed : float = navMeshAgent.speed;
	while(time > 0)
	{
		time -= Time.deltaTime;
		navMeshAgent.speed = 0;
		yield;
	}
	navMeshAgent.speed = originalSpeed;
}

public function ReturnHome(goingHome : boolean)
{
	if(goingHome)
	{
		currentState = AIState.Returning;
	}
	else
	{
		currentState = AIState.Neutral;
	}
}

protected function StateMoveSpeed(givenState : AIState)
{
	if(givenState == AIState.Returning)
	{
		destination = startPos;
		currentSpeed = walkSpeed;
	}
	else if(givenState == AIState.Chasing)
	{
		currentSpeed = chaseSpeed;
		if(isInSight)
		{
			destination = player.position;
		}
	}
	else if(givenState == AIState.Neutral)
	{
		destination = startPos;
		currentSpeed = walkSpeed;
	}
}

protected function StateMovement(speed : float, destination : Vector3, isInSight : boolean)
{
	if(MoveTo(speed, destination) && !isInSight)
	{
		if(currentState == AIState.Returning)
		{
			currentState = AIState.Neutral;
		}
		waitTimer += Time.deltaTime;
		if(waitTimer >= chaseWaitTime)
		{
			currentState = AIState.Neutral;
			waitTimer = 0f;
		}
	}
	else
		waitTimer = 0f;
}