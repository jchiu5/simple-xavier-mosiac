﻿// Type: MovementGW
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll
using UnityEngine;

public class MovementGW : MonoBehaviour
{
	public const float MAX_RUN_SPEED = 5.5f;
	public float initJumpSpeed;
	public float minJumpSpeed;
	public float jumpDamp;
	public float speedDamp;
	public float turnSmoothing;
	public bool move;
	private CharacterController _characterController;
	private Animator anim;
	private Transform camTrans;
	private bool walking;
	private Vector3 currVelocity;
	private Vector3 gravVelocity;

	public MovementGW ()
	:base()
	{
		this.initJumpSpeed = 10f;
		this.minJumpSpeed = 4f;
		this.jumpDamp = 1.1f;
		this.speedDamp = 0.1f;
		this.turnSmoothing = 15f;
		this.move = true;
	}

	public virtual void Start ()
	{
		this._characterController = (CharacterController)((Component)this).GetComponent (typeof(CharacterController));
		this.anim = (Animator)((Component)this).GetComponent (typeof(Animator));
		this.camTrans = (Transform)GameObject.FindGameObjectWithTag ("MainCamera").GetComponent (typeof(Transform));
		((Component)this).rigidbody.interpolation = ((RigidbodyInterpolation)1);
		this.currVelocity = Vector3.zero;
	}

	public virtual void Update ()
	{
		this.currVelocity = Vector3.zero;
		if (this._characterController.isGrounded) {
				this.currVelocity = (this.currVelocity + ((Vector3.down * this._characterController.stepOffset) * Time.deltaTime));
				this.gravVelocity = Vector3.zero;
		} else
		{
			this.gravVelocity = (this.gravVelocity + (Physics.gravity * Time.deltaTime));
		}
		if (Input.GetButtonDown ("Walk"))
		{
			this.walking = !this.walking;
		}
		float horizontal = GWMath.ReturnFurtherFromZero (Input.GetAxis ("Horizontal"), Input.GetAxis ("xHorizontal"));
		float vertical = GWMath.ReturnFurtherFromZero (Input.GetAxis ("Vertical"), Input.GetAxis ("xVertical"));
		this.anim.SetFloat ("Direction", horizontal);
		if ((double)horizontal != 0.0 || (double)vertical != 0.0)
		{
			float num = Mathf.Clamp(Mathf.Abs (horizontal) + Mathf.Abs (vertical), 0.0f, 1f) * MovementGW.MAX_RUN_SPEED;
			if (this.walking)
			{
				num = Mathf.Clamp (num, 0.0f, 1.5f);
			}
			this.anim.SetFloat ("Speed", num, this.speedDamp, Time.deltaTime);
		} else {
			this.anim.SetFloat ("Speed", 0.0f, this.speedDamp, Time.deltaTime);
		}
		if (anim.GetCurrentAnimatorStateInfo (0).IsName ("Base Layer.Blend Tree")) {
			currVelocity += transform.forward * anim.GetFloat ("Speed") * Time.deltaTime;
			this.Rotate (horizontal, vertical);
		}
		this.ApplyGravity ();
		this._characterController.Move (this.currVelocity);
	}

	private void Rotate (float horizontal, float vertical)
	{
		Vector3 moveDirection = new Vector3 (horizontal, 0, vertical);
		moveDirection = camTrans.TransformDirection (moveDirection);
		moveDirection.y = 0;

		if (moveDirection != Vector3.zero) {
			var targetRotation = Quaternion.LookRotation (moveDirection, Vector3.up);

			Quaternion newRotation = Quaternion.Lerp (rigidbody.rotation, targetRotation, turnSmoothing * Time.deltaTime * this.anim.GetFloat("Speed") / MovementGW.MAX_RUN_SPEED);

			rigidbody.MoveRotation (newRotation);
		}
	}

	private void ApplyGravity ()
	{
			currVelocity += gravVelocity * Time.deltaTime;
	}

	public virtual void Main ()
	{
	}
}
