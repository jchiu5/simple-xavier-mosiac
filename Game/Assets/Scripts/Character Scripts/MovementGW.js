﻿#pragma strict

// public final var MAX_RUN_SPEED : float = 10.5f;

public var MAX_RUN_SPEED : float = 5.5f;

public var initJumpSpeed : float = 10f;
public var minJumpSpeed : float = 4f;
public var jumpDamp : float = 1.1f;
public var speedDamp : float = 0.1f;
public var turnSmoothing : float = 15f;
public var move : boolean = true;

private var _characterController : CharacterController;
private var anim : Animator;
private var camTrans : Transform;
private var walking : boolean = false;
private var currVelocity : Vector3;
private var gravVelocity : Vector3;

function Start () {
	_characterController = GetComponent(CharacterController);
	anim = GetComponent(Animator);
	camTrans = GameObject.FindGameObjectWithTag("MainCamera").GetComponent(Transform);
	rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
	currVelocity = Vector3.zero;
}

function Update()
{
	currVelocity = Vector3.zero;
	
	if(_characterController.isGrounded)
	{
		currVelocity += Vector3.down * _characterController.stepOffset * Time.deltaTime;
		gravVelocity = Vector3.zero;
	}
	else
	{
		gravVelocity += Physics.gravity * Time.deltaTime;
	}
	
	if(Input.GetButtonDown("Walk"))
	{
		walking = !walking;
	}
	
	var h : float = GWMath.ReturnFurtherFromZero(Input.GetAxis("Horizontal"), Input.GetAxis("xHorizontal"));
	var v : float = GWMath.ReturnFurtherFromZero(Input.GetAxis("Vertical"), Input.GetAxis("xVertical"));

	anim.SetFloat("Direction", h);
	if(h != 0f || v != 0f)
	{
		var moveSpeed = Mathf.Clamp(Mathf.Abs(h) + Mathf.Abs(v), 0f, 1f) * MAX_RUN_SPEED;
        if(walking)
		{
			moveSpeed = Mathf.Clamp(moveSpeed,0,1.5f);
		}
        anim.SetFloat("Speed", moveSpeed, speedDamp, Time.deltaTime);
	}
	else
	{
		anim.SetFloat("Speed", 0f, speedDamp, Time.deltaTime);
	}
	ApplyGravity();
	if(anim.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Blend Tree"))
	{
		currVelocity += transform.forward * anim.GetFloat("Speed") * Time.deltaTime;
		Rotate(h, v);
	}
	_characterController.Move(currVelocity);
}

private function Rotate(horizontal : float, vertical : float)
{
	var moveDirection = new Vector3(horizontal,0, vertical);
	moveDirection = camTrans.TransformDirection(moveDirection);
	moveDirection.y = 0;
	
	if(moveDirection != Vector3.zero)
	{
	    var targetRotation = Quaternion.LookRotation(moveDirection, Vector3.up);
	    var newRotation = Quaternion.Lerp(rigidbody.rotation, targetRotation, turnSmoothing * Time.deltaTime);
	    
	    rigidbody.MoveRotation(newRotation);
	}
}

private function ApplyGravity()
{
	currVelocity += gravVelocity * Time.deltaTime;
}


public function buffSpeed()
{
MAX_RUN_SPEED = 10.5f;
// animation["attack0"].speed = 2.5;
}

public function nerfSpeed()
{
MAX_RUN_SPEED = 5.5f;
}
