﻿#pragma strict

private var anim : Animator;
private var justDied : boolean = false;

function Start () {
	anim = GetComponent(Animator);
}

function Update () {
	
}

public function Die()
{
	anim.SetFloat("Speed", 0.0f);
	if(!anim.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.death"))
	{
		anim.SetTrigger("isDead");
	}
	GetComponent(MovementGW).enabled = false;
	GetComponent(Attack).enabled = false;
	collider.enabled = false;
	GetComponent(NavMeshObstacle).enabled = false;
}