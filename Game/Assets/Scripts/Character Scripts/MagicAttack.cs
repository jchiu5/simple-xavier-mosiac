// Type: MagicAttack
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll
using UnityEngine;

public class MagicAttack : MonoBehaviour
{
	public GameObject explosionPrefab;
	private Animator anim;
	public Transform handPosition;

	// An attempt to edit this easier
	public float magicDmg = 10f;

	public virtual void Start ()
	{
			this.anim = (Animator)((Component)this).GetComponent (typeof(Animator));
	}

	public virtual void Update ()
	{
			if ((double)Time.timeScale <= 0.0 || !Input.GetButtonDown ("SilasSecondaryAttack") && (double)Input.GetAxis ("xSilasSecondaryAttack") <= 0.0)
					return;
			this.SpellAttack ();
	}

	private void SpellAttack ()
	{
			this.anim.SetTrigger ("SpellAttack");
	}

	public virtual void CreateExplosion (AnimationEvent animationEvent)
	{
			Object.Instantiate ((Object)this.explosionPrefab, handPosition.position, Quaternion.identity);
	}

	public virtual void Main ()
	{
	}
}
