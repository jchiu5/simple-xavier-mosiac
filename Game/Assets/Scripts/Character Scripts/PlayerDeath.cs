// Type: PlayerDeath
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
  private Animator anim;
  private bool justDied;

  public virtual void Start()
  {
    this.anim = (Animator) ((Component) this).GetComponent(typeof (Animator));
  }

  public virtual void Update()
  {
  }

  public virtual void Die()
  {
    this.anim.SetFloat("Speed", 0.0f);
    AnimatorStateInfo animatorStateInfo = this.anim.GetCurrentAnimatorStateInfo(0);
    // ISSUE: explicit reference operation
    if (!((AnimatorStateInfo) @animatorStateInfo).IsName("Base Layer.death"))
      this.anim.SetTrigger("isDead");
    ((Behaviour) ((Component) this).GetComponent(typeof (MovementGW))).enabled = (false);
    ((Behaviour) ((Component) this).GetComponent(typeof (Attack))).enabled = (false);
    ((Component) this).collider.enabled = (false);
    ((Behaviour) ((Component) this).GetComponent(typeof (NavMeshObstacle))).enabled = (false);
  }

  public virtual void Main()
  {
  }
}
