﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : Health {
	public UISlider healthSlider;

	public override void TakeDamage (float damage)
	{
		base.TakeDamage (damage);
		healthSlider.sliderValue = health / maxHealth;
	}


}
