// Type: Health
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class Health : MonoBehaviour
{
  public float health;
  public bool isDead;
  [HideInInspector]
  public float maxHealth;
	// Rage Mechanic Edits
	public float rage; // Will have a cap of 10.
	public float rageIncreaseAmount; // Changes the amount of rage recieved when he takes damage
	// public GameObject RageIndicator;
	public bool isMaxRage;
	public bool indicatorActive;

  public GameObject bloodParticles;
  private Animator anim;
  private CharacterController _characterController;
  private CapsuleCollider _capsuleCollider;
  private Vector3 heightOffset;
  private AudioSource _audioSource;


	public GameObject player;
	public GameObject playerClone;







  public Health()
		:base()
  {
    this.health = 100f;
    this.heightOffset = Vector3.zero;
		// Rage Mechanic Edits
		this.rage = 0f; // Default value of rage should be 0. Max is 10.
		this.rageIncreaseAmount = 2f; // tentativly set to two for testing purposes.
		this.isMaxRage = false;
		this.indicatorActive = false;
		// this.player = Resources.Load("Indicator") as GameObject;

		 // this.go = Instantiate(Resources.Load("Indicator")) as GameObject; 
  }

  public virtual void Start()
  {
		maxHealth = health;
		bloodParticles = (GameObject)Instantiate((Object)bloodParticles);
		_characterController = GetComponent<CharacterController>();
		_capsuleCollider = GetComponent<CapsuleCollider>();
		_audioSource = GetComponent<AudioSource>();
		anim = GetComponent<Animator>();
		if(_capsuleCollider)
			heightOffset.y = _capsuleCollider.height / 2;
		if(_characterController)
			heightOffset.y = _characterController.height / 2;
  }

  public virtual void Update()
  {
		if (isMaxRage && !indicatorActive)
		{
		
			 //Indicator = (GameObject) Instantiate(RageIndicator, gameObject.transform.position, Quaternion.identity);


			playerClone =  Instantiate(player) as GameObject;
			playerClone.transform.parent = transform;
			indicatorActive = true;
		}


	if (indicatorActive)
		{
		
		}


	
	if ((double) this.health <= 0.0)
    {
      this.isDead = true;
      ((Component) this).SendMessage("Die");
    }
    else
      this.isDead = false;
  }

  public virtual void TakeDamage(float damage)
  {
		health = health - damage;

		// Rage Mechanic Edits

		if (this.rage < 10.0)
		{
			IncreaseRageByAttack(2);
		// Debug.Log("damaged, increment rage");

		}

		if (this.rage >= 10.0)
		{
			isMaxRage = true;
			_audioSource.Play();
			if(anim)
				anim.SetTrigger("TakeDamage");
			if(!_audioSource.isPlaying)
				_audioSource.Play();
		}
  }

	public virtual void IncreaseRageByAttack(float rageAmount)
	{


	
		if (rage < 10.0)
		{
		rage += rageAmount;
		gameObject.SendMessage ("updateMDamage", rage, SendMessageOptions.DontRequireReceiver);
		Debug.Log("Silas: Rage is now: " + this.rage);
		}
		if ((double)this.rage >= 10.0)
		{
			gameObject.SendMessage ("updateMDamage", rage, SendMessageOptions.DontRequireReceiver); // not working
			gameObject.SendMessage("buffSpeed", SendMessageOptions.DontRequireReceiver);
			Debug.Log("Rage is now max: " + rage);
			isMaxRage = true;
		}


	}


	public virtual void resetRage()
	{

		rage = 0;
		isMaxRage = false;
		gameObject.SendMessage("nerfSpeed", SendMessageOptions.DontRequireReceiver);
	}

  public virtual void Main()
  {
  }
}
