﻿// shouldn't we have switched to the CS version by now?


#pragma strict
public class Attack extends BaseAttack {

public var chanceToHit : float = 0.8f;
public var angleOfAttack : float = 90f;

protected var equippedWeapon : Item;

private var anim : Animator;
private var radius : double = 5.0;
private var canAttack : boolean;

override function Start () {
	super.Start();
	
	anim = gameObject.GetComponent(Animator);
	equippedWeapon = GetComponent(Inventory).equippedWeapon;
    canAttack = true;
}

override function Update () {
	if(Time.timeScale > 0)
	{
		if(Input.GetButtonDown("SilasPrimaryAttack") || Input.GetAxis("xSilasPrimaryAttack") < 0)
		{
			BeginAttack();
		}
 	}
}

protected function BeginAttack()
{
	// Debug.Log ("Using the js script");
	anim.SetTrigger("Attack");
	objectsHit.Clear();
}

public function Attacking(animationEvent : AnimationEvent)
{
	if(canAttack)
	{
		var ray : Vector3;
		for(var i = 0; i < objectsInRange.Count; ++i)
		{
			if(!objectsHit.Contains(objectsInRange[i].gameObject))
			{
				//check if in the thing
				ray = objectsInRange[i].transform.position - transform.position;
				if(Vector3.Angle(transform.forward, ray) < angleOfAttack)
				{
					//health decrement according to GDD
					if(DoesItHit(chanceToHit))
					{
						objectsInRange[i].gameObject.SendMessage("TakeDamage", damage, SendMessageOptions.DontRequireReceiver);
						objectsHit.Add(objectsInRange[i].gameObject);
						
						// Rage Mechanic Edits
						// Send message to increase Rage.
						
						
						//IncreaseRageByAttack(1);
						gameObject.SendMessage ("IncreaseRageByAttack", 1.0, SendMessageOptions.DontRequireReceiver);
						
					}
				}
			}
		}
	}
}

public function CanAttack(animationEvent : AnimationEvent)
{
	if(animationEvent.intParameter == 0)
	{
		canAttack = false;
	}
	else
	{
		canAttack = true;
	}
}

private function DoesItHit(chanceToHit : float)
{
	return (Random.value <= chanceToHit);
}
}