﻿#pragma strict

public var explosionPrefab : GameObject;

public var handPosition : Transform;
private var anim : Animator;

function Start () {
	anim = GetComponent(Animator);
}

function Update () {
	if(Time.timeScale > 0)
	{
		if(Input.GetButtonDown("SilasSecondaryAttack") || Input.GetAxis("xSilasSecondaryAttack") > 0)
		{
			SpellAttack();
		}
	}
}

private function SpellAttack()
{
	anim.SetTrigger("SpellAttack");
	gameObject.SendMessage("resetRage", SendMessageOptions.DontRequireReceiver);
	Debug.Log("Rage should be reset, speed should be normal.");
}

public function CreateExplosion(animationEvent : AnimationEvent)
{
	GameObject.Instantiate(explosionPrefab, handPosition.position, Quaternion.identity);
}