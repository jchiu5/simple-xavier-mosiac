#pragma strict

public var health : float = 100f;
public var isDead = false;
@HideInInspector
public var maxHealth : float;
public var bloodParticles : ParticleSystem;


private var anim : Animator;
private var _characterController : CharacterController;
private var _capsuleCollider : CapsuleCollider;
private var heightOffset : Vector3 = Vector3.zero;
private var _audioSource : AudioSource;

function Start () {
	maxHealth = health;
	bloodParticles = Instantiate(bloodParticles);
	_characterController = GetComponent(CharacterController);
	_capsuleCollider = GetComponent(CapsuleCollider);
	_audioSource = GetComponent(AudioSource);
	anim = GetComponent(Animator);
	if(_capsuleCollider)
		heightOffset.y = _capsuleCollider.height / 2;
	if(_characterController)
		heightOffset.y = _characterController.height / 2;
}

function Update () {
	if(health <= 0)
	{
		isDead = true;
		SendMessage("Die");
	}
	else
	{
		isDead = false;
	}
}

public function TakeDamage(damage : float)
{
	health = health - damage;
	Debug.Log("Took damage here");
	
	if(!isDead)
	{
		bloodParticles.transform.position = gameObject.transform.position + heightOffset;
		bloodParticles.Play();
		_audioSource.Play();
		if(anim)
			anim.SetTrigger("TakeDamage");
		if(!_audioSource.isPlaying)
			_audioSource.Play();
		
	}
}