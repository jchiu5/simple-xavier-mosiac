﻿// Type: Attack
// Assembly: Assembly-UnityScript, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E702BA7A-E866-4AAA-AA50-E7D3896A2E42
// Assembly location: C:\Users\Player1\Desktop\stuff\Jay's Realm\xavier-mosaic\Game\Library\ScriptAssemblies\Assembly-UnityScript.dll

using UnityEngine;

public class Attack : BaseAttack
{
  public float chanceToHit;
  public float angleOfAttack;
  protected Item equippedWeapon;
  private Animator anim;
  private double radius;
  private bool canAttack;

  public Attack()
  {
    this.chanceToHit = 0.8f;
    this.angleOfAttack = 90f;
    this.radius = 5.0;
  }

  public override void Start()
  {
    base.Start();
    this.anim = (Animator) ((Component) this).gameObject.GetComponent(typeof (Animator));
    this.equippedWeapon = ((Inventory) ((Component) this).GetComponent(typeof (Inventory))).equippedWeapon;
    this.canAttack = true;
  }

  public override void Update()
  {
    if (Time.timeScale <= 0.0f || !Input.GetButtonDown("SilasPrimaryAttack") && Input.GetAxis("xSilasPrimaryAttack") >= 0.0f)
      return;
    this.BeginAttack();
  }

  protected virtual void BeginAttack()
	{

		// Debug.Log ("Using the cs script");
		this.anim.SetTrigger("Attack");
    this.objectsHit.Clear();
  }

  public virtual void Attacking(AnimationEvent animationEvent)
  {
    if (!this.canAttack)
      return;
    for (int index = 0; index < this.objectsInRange.Count; ++index)
    {
      if (!this.objectsHit.Contains(((Component) this.objectsInRange[index]).gameObject) && (double) Vector3.Angle(((Component) this).transform.forward, (((Component) this.objectsInRange[index]).transform.position - ((Component) this).transform.position)) < (double) this.angleOfAttack && this.DoesItHit(this.chanceToHit))
      {
        ((Component) this.objectsInRange[index]).gameObject.SendMessage("TakeDamage", (object) this.damage, (SendMessageOptions) 1);
        this.objectsHit.Add(((Component) this.objectsInRange[index]).gameObject);
      }
    }
  }

  public virtual void CanAttack(AnimationEvent animationEvent)
  {
    if (animationEvent.intParameter == 0)
      this.canAttack = false;
    else
      this.canAttack = true;
  }

  private bool DoesItHit(float chanceToHit)
  {
    return (double) Random.value <= (double) chanceToHit;
  }
}
