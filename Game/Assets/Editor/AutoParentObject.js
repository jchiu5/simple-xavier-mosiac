﻿#pragma strict

@MenuItem ("GameObject/Create Empty Child #&n")
static function BringMeBackFromTheEdgeOfMadness()
{
	var go : GameObject = new GameObject("GameObject");
	if(Selection.activeTransform != null)
	{
		go.transform.parent = Selection.activeTransform;
	}
}