#pragma strict

public var cannotHitTag : List.<String>;
public var damage : float = 10f;
public var chanceToHit : float = 0.8f;
public var angleOfAttack : float = 90f;

protected var anim : Animator;
protected var equippedWeapon : Item;

private var radius : double = 5.0;
private var objectsInRange : List.<Collider>;
private var objectsHit : List.<GameObject>;
private var canAttack : boolean;

function Start () {
	anim = GetComponent(Animator);
	equippedWeapon = GetComponent(Inventory).equippedWeapon;
    objectsHit = new List.<GameObject>();
    objectsInRange = new List.<Collider>();
    canAttack = true;
}

function Update () {
	if(Time.timeScale > 0)
	{
		if(Input.GetButtonDown("SilasPrimaryAttack") || Input.GetAxis("xSilasPrimaryAttack") < 0 || Input.GetButtonDown("Skill1") )
		{
			Attack();
		}
		if(Input.GetButtonDown("SilasSecondaryAttack") || Input.GetAxis("xSilasSecondaryAttack") > 0)
		{
			
		}
 	}
}

function OnTriggerStay(other : Collider)
{
	if(!objectsInRange.Contains(other) && !cannotHitTag.Contains(other.collider.tag) && !other.collider.isTrigger)
		objectsInRange.Add(other);
}

function OnTriggerExit(other : Collider)
{
	objectsInRange.Remove(other);
}

protected function Attack()
{
	anim.SetTrigger("Attack");
	objectsHit.Clear();
}

public function Attacking(animationEvent : AnimationEvent)
{
	if(canAttack)
	{
		var ray : Vector3;
		for(var i = 0; i < objectsInRange.Count; ++i)
		{
			if(!objectsHit.Contains(objectsInRange[i].gameObject))
			{
				//check if in the thing
				ray = objectsInRange[i].transform.position - transform.position;
				if(Vector3.Angle(transform.forward, ray) < angleOfAttack)
				{
					//health decrement according to GDD
					if(DoesItHit(chanceToHit))
					{
						objectsInRange[i].gameObject.SendMessage("TakeDamage", damage, SendMessageOptions.DontRequireReceiver);
						objectsHit.Add(objectsInRange[i].gameObject);
					}
				}
			}
		}
	}
}

public function CanAttack(animationEvent : AnimationEvent)
{
	if(animationEvent.intParameter == 0)
	{
		canAttack = false;
	}
	else
	{
		canAttack = true;
	}
}

private function DoesItHit(chanceToHit : float)
{
	return (Random.value <= chanceToHit);
}