﻿#pragma strict

public var initJumpSpeed : float = 10f;
public var minJumpSpeed : float = 4f;
public var jumpDamp : float = 1.1f;
public var speedDamp : float = 0.1f;
public var turnSmoothing : float = 15f;
public var move : boolean = true;

private var _characterController : CharacterController;
private var anim : Animator;
private var camTrans : Transform;
private var walking : boolean = false;
private var currJumpSpeed : float;
private var jumping : boolean;
private var currVelocity : Vector3;
private var gravVelocity : Vector3;

function Start () {
	_characterController = GetComponent(CharacterController);
	anim = GetComponent(Animator);
	camTrans = GameObject.FindGameObjectWithTag("MainCamera").GetComponent(Transform);
	rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
	jumping = false;
	currVelocity = Vector3.zero;
	currJumpSpeed = initJumpSpeed;
	
}

function Update()
{
	var jump : boolean = (Input.GetButtonDown("Jump") || Input.GetButtonDown("xA"));
	
	currVelocity = Vector3.zero;
	
	if(_characterController.isGrounded)
	{
		currVelocity += Vector3.down * _characterController.stepOffset * Time.deltaTime;
		gravVelocity = Vector3.zero;
		jumping = jump;
		if(jump)
		{
			currJumpSpeed = initJumpSpeed;
		}
	}
	else
	{
		gravVelocity += Physics.gravity * Time.deltaTime;
	}
	
	if(jumping)
	{
		currVelocity += Vector3.up * currJumpSpeed * Time.deltaTime;
		currJumpSpeed = Mathf.Lerp(currJumpSpeed, minJumpSpeed, Time.deltaTime * jumpDamp);
		
		var startLine : Vector3 = transform.position + _characterController.center;
		startLine.y += _characterController.height / 2;
		if(Physics.Linecast(startLine, startLine + Vector3.up * 0.1f))
		{
			currJumpSpeed = 0;
		}
	}
	
	if(Input.GetButtonDown("Walk"))
	{
		walking = !walking;
	}
	
	var h : float = GWMath.ReturnFurtherFromZero(Input.GetAxis("Horizontal"), Input.GetAxis("xHorizontal"));
	var v : float = GWMath.ReturnFurtherFromZero(Input.GetAxis("Vertical"), Input.GetAxis("xVertical"));

	anim.SetFloat("Direction", h);
	if(h != 0f || v != 0f)
	{
		Rotate(h, v);
		var moveSpeed = Mathf.Clamp(Mathf.Abs(h) + Mathf.Abs(v), 0f, 1f) * 5.5f;
        if(walking)
		{
			moveSpeed = Mathf.Clamp(moveSpeed,0,1.5f);
		}
        anim.SetFloat("Speed", moveSpeed, speedDamp, Time.deltaTime);
	}
	else
	{
		anim.SetFloat("Speed", 0f, speedDamp, Time.deltaTime);
	}
	if(anim.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Blend Tree"))
	{
		currVelocity += transform.forward * anim.GetFloat("Speed") * Time.deltaTime;
	}
	ApplyGravity();
	_characterController.Move(currVelocity);

////// 
if(Input.GetButtonDown("skill1"))
{
Debug.Log("skill 1, do something");
Skill1();
}

if(Input.GetButtonDown("skill2"))
{
Debug.Log("skill 2, do something");
Skill2();
}

if(Input.GetButtonDown("skill3"))
{
Debug.Log("skill 3, do something");
Skill3();
}

if(Input.GetButtonDown("skill4"))
{
Debug.Log("skill 4, do something");
Skill4();
}

if(Input.GetButtonDown("skill5"))
{
Debug.Log("skill 5, do something");
Skill5();
}

}



protected function Skill1()
{
	anim.SetTrigger("Skill1");
	
}

protected function Skill2()
{
	anim.SetTrigger("Skill2");
	
}


protected function Skill3()
{
	anim.SetTrigger("Skill3");
	
}

protected function Skill4()
{
	anim.SetTrigger("Skill4");
	
}

protected function Skill5()
{
	
	anim.SetTrigger("Skill5");

	
}

////



private function Rotate(horizontal : float, vertical : float)
{
	var moveDirection = new Vector3(horizontal,0, vertical);
	moveDirection = camTrans.TransformDirection(moveDirection);
	moveDirection.y = 0;
	
	if(moveDirection != Vector3.zero)
	{
	    var targetRotation = Quaternion.LookRotation(moveDirection, Vector3.up);
	    
	    var newRotation = Quaternion.Lerp(rigidbody.rotation, targetRotation, turnSmoothing * Time.deltaTime);
	    
	    rigidbody.MoveRotation(newRotation);
	}
}

private function ApplyGravity()
{
	currVelocity += gravVelocity * Time.deltaTime;
}